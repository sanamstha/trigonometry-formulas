package com.ktmstudio.sanam.allformulasguide.Model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by backtrack on 6/22/2017.
 */
public class Filename implements Parcelable{

     String filename;
     String title;

    public Filename() {
    }
    private Filename(Parcel in) {
        filename = in.readString();
        title = in.readString();
    }

    public Filename(String filename, String title) {
        this.filename = filename;
        this.title = title;
    }

    public String getFilename() {
        return filename;
    }


    public String getTitle() {
        return title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(filename);
        parcel.writeString(title);
    }

    public static final Parcelable.Creator<Filename> CREATOR
            = new Parcelable.Creator<Filename>() {
        public Filename createFromParcel(Parcel in) {
            return new Filename(in);
        }

        public Filename[] newArray(int size) {
            return new Filename[size];
        }
    };

}
