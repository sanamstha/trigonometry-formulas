package com.ktmstudio.sanam.allformulasguide;


import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.ktmstudio.sanam.allformulasguide.Adapter.RecyclerAdapter;
import com.ktmstudio.sanam.allformulasguide.Model.Filename;
import com.ktmstudio.sanam.allformulasguide.Model.NotesData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomePage extends Fragment implements RecyclerAdapter.recycleClicked {

    RecyclerView recyclerView;
    private RecyclerAdapter recyclerAdapter;
    private TypedArray img;
    ArrayList<NotesData> notesDataArrayList = new ArrayList<>();
    InterstitialAd mInterstitialAd;
    private int indexPosition = 0;
    private boolean adclosed = false;


    public HomePage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        adclosed = false;
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
//   Ads request
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Load ads into Interstitial Ads
        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                adclosed = true;


            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }
        });

        recyclerView = view.findViewById(R.id.recycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        notesDataArrayList.clear();
        img = getResources().obtainTypedArray(R.array.imagename);


        recyclerAdapter = new RecyclerAdapter(getdata(), img, getContext());
        recyclerAdapter.setRecycleClicked(this);


        recyclerView.setAdapter(recyclerAdapter);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adclosed) {
            showNextActivity(indexPosition);
        }
    }

    public void showNextActivity(int position) {
        if (notesDataArrayList.get(position).isAccess()) {

            SecondPage secondPage = new SecondPage();
            Bundle bundle = new Bundle();
            ArrayList<Filename> data = new ArrayList<>(notesDataArrayList.get(position).getFilename());
            bundle.putParcelableArrayList("subdata", data);
            secondPage.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.container, secondPage).addToBackStack(null).commit();

        } else {
            DataViewer dataViewer = new DataViewer();

            Bundle bundle = new Bundle();
            bundle.putString("filename", notesDataArrayList.get(position).getFilename().get(0).getFilename());
            dataViewer.setArguments(bundle);

            getFragmentManager().beginTransaction().replace(R.id.container, dataViewer).addToBackStack(null).commit();
        }
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("advdetail.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public ArrayList<NotesData> getdata() {
        try {
            JSONArray m_jArry = new JSONArray(loadJSONFromAsset());

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);


                String title = jo_inside.getString("title");
                String subtitle = jo_inside.getString("subtitle");
                boolean access = jo_inside.getBoolean("access");
                NotesData notesData = new NotesData();
                notesData.setTitle(title);
                notesData.setSubtitle(subtitle);
                notesData.setAccess(access);


                JSONArray filename = jo_inside.getJSONArray("data");
                ArrayList<Filename> filenameArrayList = new ArrayList<>();
                for (int j = 0; j < filename.length(); j++) {
                    JSONObject name = filename.getJSONObject(j);
                    String pdf = name.getString("filename");
                    String fileTitle = name.getString("title");
                    Filename filename1 = new Filename(pdf, fileTitle);
                    filenameArrayList.add(filename1);

                    //  Log.d("Details-->",pdf);
                }
                notesData.setFilename(filenameArrayList);
                notesDataArrayList.add(notesData);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return notesDataArrayList;
    }

    @Override
    public void onRecycleviewClicked(View v, int position) {


        indexPosition = position;

        if (position % 3 == 0) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                showNextActivity(position);
            }
        } else {
            showNextActivity(position);
        }


    }


}








