package com.ktmstudio.sanam.allformulasguide;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class DataViewer extends Fragment {
    ProgressBar progressBar;

    public DataViewer() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_data_viewer, container, false);
        progressBar = view.findViewById(R.id.progressBar2);
        String filename = getArguments().getString("filename");
        String addOn = String.format("<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=0.6, user-scalable=no\"></head><img src=\"%s\"></html", filename);
        WebView wv = view.findViewById(R.id.webview);
        wv.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.setInitialScale(1);
        wv.getSettings().setDisplayZoomControls(false);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.loadDataWithBaseURL("file:///android_asset/", addOn, "text/html", "utf-8", null);

        wv.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                if (newProgress==100){

                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                }

            }
        });
        return view;
    }

}
