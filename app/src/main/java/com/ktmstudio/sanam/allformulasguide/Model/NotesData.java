package com.ktmstudio.sanam.allformulasguide.Model;


import java.util.List;

/**
 * Created by backtrack on 6/22/2017.
 */
public class NotesData {



    private List<Filename> filename;
    private String title;
    private String subtitle;
    private boolean access;


    public List<Filename> getFilename() {
        return filename;
    }

    public void setFilename(List<Filename> filename) {
        this.filename = filename;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }



    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }


    public boolean isAccess() {
        return access;
    }

    public void setAccess(boolean access) {
        this.access = access;
    }
}

