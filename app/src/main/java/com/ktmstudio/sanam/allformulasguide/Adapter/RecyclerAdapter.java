package com.ktmstudio.sanam.allformulasguide.Adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktmstudio.sanam.allformulasguide.Model.NotesData;
import com.ktmstudio.sanam.allformulasguide.R;

import java.util.ArrayList;

/**
 * Created by backtrack on 6/18/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {
    // A menu item view type.
    private static final int MENU_ITEM_VIEW_TYPE = 0;

    // The Native Express ad view type.
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;
    Context mContext;
    ArrayList<NotesData> notesDataArrayList;
    TypedArray img;
    recycleClicked recycleClicked;
    public RecyclerAdapter(ArrayList<NotesData> notesDataArrayList,TypedArray img,Context context) {
        this.notesDataArrayList = notesDataArrayList;
        this.img = img;
        this.mContext = context;

    }


    public void setRecycleClicked(recycleClicked recycleClicked){
        this.recycleClicked = recycleClicked;
    }

    @Override
    public RecyclerAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {



                View mv = LayoutInflater.from(parent.getContext()).inflate(R.layout.homerow,parent,false);
                return new Holder(mv);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.Holder holder, int position) {

                Holder holder1 = (Holder) holder;
                NotesData notesData = notesDataArrayList.get(position);

                holder1.title.setText(notesData.getTitle());
                holder1.subtitle.setText(notesData.getSubtitle());
                holder1.homeimageView.setBackgroundColor(img.getColor(position,0));


    }

    @Override
    public int getItemCount() {

        return notesDataArrayList.size();
    }






    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{


        ImageView homeimageView;
        TextView title;
        TextView subtitle;

        public Holder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            title = itemView.findViewById(R.id.homeTitle);
            homeimageView = itemView.findViewById(R.id.homeimg);
            subtitle = itemView.findViewById(R.id.homeSubtitle);


        }

        @Override
        public void onClick(View view) {
            recycleClicked.onRecycleviewClicked(view,getAdapterPosition());
        }
    }



    public interface recycleClicked{

         void onRecycleviewClicked(View v,int position);
    }


}
