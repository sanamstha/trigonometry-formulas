package com.ktmstudio.sanam.allformulasguide;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ktmstudio.sanam.allformulasguide.Adapter.SubDataAdapter;
import com.ktmstudio.sanam.allformulasguide.Model.Filename;


import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondPage extends Fragment implements SubDataAdapter.onSubItemclicked  {

    RecyclerView recyclerView;
    SubDataAdapter recyclerAdapter;

    ArrayList<Filename> filenameArrayList  ;
    public SecondPage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//
         filenameArrayList = getArguments().getParcelableArrayList("subdata");
        View view = inflater.inflate(R.layout.fragment_second_page, container, false);
        recyclerView = view.findViewById(R.id.secrecycleview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerAdapter = new SubDataAdapter(filenameArrayList);
        recyclerAdapter.setOnSubItemClicked(this);
        recyclerView.setAdapter(recyclerAdapter);
        return view;
    }

    @Override
    public void onSubClicked(View v, int position) {
        DataViewer dataViewer = new DataViewer();

        Bundle bundle = new Bundle();
        bundle.putString("filename",filenameArrayList.get(position).getFilename());
        dataViewer.setArguments(bundle);

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,dataViewer).addToBackStack(null).commit();
    }
}
