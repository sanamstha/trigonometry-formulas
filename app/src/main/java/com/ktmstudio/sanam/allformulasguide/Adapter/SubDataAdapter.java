package com.ktmstudio.sanam.allformulasguide.Adapter;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktmstudio.sanam.allformulasguide.Model.Filename;
import com.ktmstudio.sanam.allformulasguide.R;

import java.util.ArrayList;

/**
 * Created by backtrack on 6/22/2017.
 */

public class SubDataAdapter extends RecyclerView.Adapter<SubDataAdapter.Holder> {

    ArrayList<Filename> arrayList;
    onSubItemclicked subItemclicked;
    TypedArray subimg;

    public SubDataAdapter(ArrayList<Filename> arrayList) {
        this.arrayList = arrayList;

    }

    public  void setOnSubItemClicked(onSubItemclicked subItemClicked){
        this.subItemclicked = subItemClicked;

    }
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        subimg = parent.getContext().getResources().obtainTypedArray(R.array.imagename);
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sublistrow,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Filename filename = arrayList.get(position);

        holder.imageView.setBackgroundColor(subimg.getColor(position,0));
        holder.textView.setText(filename.getTitle());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textView;
        ImageView imageView;

        public Holder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.subrowTitle);
            imageView = itemView.findViewById(R.id.subimg);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            subItemclicked.onSubClicked(view,getAdapterPosition());
        }
    }

    public interface onSubItemclicked{

        void onSubClicked(View v,int position);
    }
}
